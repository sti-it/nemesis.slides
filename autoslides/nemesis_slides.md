

# Nemesis


---

# What is Nemesis

Nemesis is the [STI-IT](http://sti.epfl.ch/it) cluster in Neuchâtel.

## Cluster details
* 33 x1U Twin servers (Dual Node)
* &nbsp;2 x1U PowerEdge R200
* &nbsp;4 x1U Switch 48 ports [Netgear ProSafe GS748T](http://support.netgear.fr/product/GS748Tv1)

---

# Cluster OS

* CoreOS is an open-source lightweight operating system based on the Linux kernel and designed for providing infrastructure to clustered deployments, while focusing on automation, ease of applications deployment, security, reliability and scalability. As an operating system, CoreOS provides only the minimal functionality required for deploying applications inside software containers, together with built-in mechanisms for service discovery and configuration sharing.


* Nemesis runs 100% CoreOS nodes

![CoreOS Logo](img/coreos-logo-200.png)

---

# Docker
* Docker is an open-source project that makes creating and managing Linux containers really easy. Containers are like extremely lightweight VMs – they allow code to run in isolation from other containers but safely share the machine’s resources, all without the overhead of a hypervisor.

![Docker Logo](img/docker-logo-200.png)

---

# Docker + CoreOS

CoreOS is pre-configured with popular tools for running Linux containers. 

![Host Diagram](img/CoreOS-Host-Diagram.png)

---

# Cluster managment 1

![Foreman Hosts](img/foreman-hosts.png)
![Foreman Logo](img/foreman-logo-200.png)

---

# Cluster managment 2

* ..
* ...
* ....

---

# More infos

[https://gitlab.epfl.ch/sti-it/ops.nemesis](https://gitlab.epfl.ch/sti-it/ops.nemesis)

& 

[https://github.com/epfl-sti/cluster.foreman](https://github.com/epfl-sti/cluster.foreman)

---
